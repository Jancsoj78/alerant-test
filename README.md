## Alerant nexus demo

## step 1
#### use a docker compatible ubuntu 22.04 virtualbox VM and portforward 8081 port
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/vm-portforward.png)](https://gitlab.com/Jancsoj78/alerant-test)

#### add insecure registry to docker (/etc/docker/daemon.json) (file is attached)

## step 2
#### run docker nexus  with init script and password script

``` docker run -d -p 8081:8081 -p 5000:5000 --restart always --name nexus-alerant sonatype/nexus3 ```

#### check admin password to first run

``` docker exec nexus-alerant cat /nexus-data/admin.password ```

## step 3

#### create user in nexus
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/create-user.png)](https://gitlab.com/Jancsoj78/alerant-test)
#### create docker hosted repo in nexus
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/create-repo.png)](https://gitlab.com/Jancsoj78/alerant-test)
#### create localhost config in nexus
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/localhost-config.png)](https://gitlab.com/Jancsoj78/alerant-test)
#### create localhost v2 docker bearer realm enable
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/docker-bearer-enable.png)](https://gitlab.com/Jancsoj78/alerant-test)

## step 4
#### build app image

use build script in /demo-app folder 

## step 5 
#### login to the repo
``` echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin ```

#### push to the repo

``` docker push localhost:5000/alerant/alerant-demo:$release ``` 

## accepted result: uploaded docker image to nexus:
[![N|Solid](https://gitlab.com/Jancsoj78/alerant-test/-/raw/main/uploaded-image.png)](https://gitlab.com/Jancsoj78/alerant-test)


## JVM memory reservations
 ``` docker run -d -p 8081:8081 --name nexus -e MAX_HEAP=768m sonatype/nexus ``` 

## prepare to backup

 ``` mkdir /some/dir/nexus-data && chown -R 200 /some/dir/nexus-data  ```
 ``` docker run -d -p 8081:8081 --name nexus -v /some/dir/nexus-data:/sonatype-work sonatype/nexus  ```
